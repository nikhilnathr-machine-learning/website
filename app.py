from flask import Flask, render_template, request
import joblib
import sys

sys.path.append('./demo/sentiment-classifier')

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

@app.route("/andrew-ng-coursera/<string:name>", methods=["GET"])
def andrew_ng_coursera(name):
    if (name == "ex1"):
        return render_template("andrew-ng-coursera/ex1.html")
    elif (name == "ex1_multi"):
        return render_template("andrew-ng-coursera/ex1_multi.html")
    elif (name == "ex2"):
        return render_template("andrew-ng-coursera/ex2.html")
    elif (name == "ex2_reg"):
        return render_template("andrew-ng-coursera/ex2_reg.html")
    elif (name == "ex3"):
        return render_template("andrew-ng-coursera/ex3.html")
    elif (name == "ex3_nn"):
        return render_template("andrew-ng-coursera/ex3_nn.html")
    elif (name == "ex4"):
        return render_template("andrew-ng-coursera/ex4.html")
    elif (name == "ex5"):
        return render_template("andrew-ng-coursera/ex5.html")
    
    return not_found(404)

@app.route("/keith-galli-sklearn/<string:name>", methods=["GET"])
def keith_galli_sklearn(name):
    if (name == "sentiment-classifier"):
        return render_template("keith-galli-sklearn/sentiment-classifier.html")
    elif (name == "category-classifier"):
        return render_template("keith-galli-sklearn/category-classifier.html")

    return not_found(404)

@app.route("/keith-galli-sklearn/demo/<string:name>", methods=["GET", "POST"])
def keith_galli_sklearn_demo(name):
    if request.method == "GET":
        if (name == "sentiment-classifier"):
            return render_template("keith-galli-sklearn/demo/sentiment-classifier.html")
        elif (name == "category-classifier"):
            return render_template("keith-galli-sklearn/demo/category-classifier.html")

    if request.method == "POST":
        if (name == "sentiment-classifier"):
            content = extract_post_parameters(request, ["review"])
            review = [content["review"]]
                
            if len(review) < 1:
                return "Please give input", 500 
            vectorizer = joblib.load("demo/sentiment-classifier/vectorizer.pkl")
            classifier = joblib.load("demo/sentiment-classifier/classifier.pkl")
            x_test = vectorizer.transform(review)
            sentiment = classifier.predict(x_test)
            return sentiment[0]
        elif (name == "category-classifier"):
            content = extract_post_parameters(request, ["review"])
            review = [content["review"]]
                
            if len(review) < 1:
                return "Please give input", 500 
            vectorizer = joblib.load("demo/category-classifier/vectorizer.pkl")
            classifier = joblib.load("demo/category-classifier/classifier.pkl")
            x_test = vectorizer.transform(review)
            sentiment = classifier.predict(x_test)
            return sentiment[0]
    return not_found(404)

@app.errorhandler(404) 
def not_found(e):
  return "page not found", 404
  
def extract_post_parameters(request, params):
    content = request.get_json(silent=True)
    if content == None:
        content = request.form
    res = {}
    for p in params:
        if p in params:
            res[p] = content[p]
        else:
            return "Inputs not valid", 400
    return res


if __name__ == "__main__":
    app.run()